const scroll = () => {
    window.onscroll = () => {stickyFun()};
    const header =  document.querySelector('.js-header');
    const logo = document.querySelector('.js-logo');
    const sticky =  header.offsetTop;
    function stickyFun() {
        if(window.pageYOffset > sticky) {
            header.classList.add('fixed');
            logo.classList.add('active');
        } else {
            header.classList.remove('fixed');
            logo.classList.remove('active');
        }
    }
}
scroll();
const openMenu = () => {
    const iconMenu = document.querySelector('.js-burger');
    if(iconMenu) {
        const menuBody = document.querySelector('.js-burger-menu');
        const logo = document.querySelector('.js-logo');
        const noScroll = document.querySelector('.js-wrapper');
        iconMenu.addEventListener('click', () => {
            iconMenu.classList.toggle('active');
            menuBody.classList.toggle('active');
            noScroll.classList.toggle('active');
            
            if(window.pageYOffset < 70) {
               logo.classList.toggle('active');
               
            }
        });
    }
}
openMenu();
const langLink = () => {
    const items = document.querySelectorAll('.js-lang');
    if(items) {
        items.forEach(item => {
            item.addEventListener('click', langToggle)
        });
        function langToggle (){
            items.forEach(item => {
                item.classList.remove('active');
            });
            this.classList.add('active');
        }
    }
}
langLink();
const activeLink = () => {
    const items = document.querySelectorAll('.js-item');
    if(items) {
        const assortment = document.querySelectorAll('.js-assortment');
        const content = document.querySelector('.js-content');
        const links = document.querySelectorAll('.js-link');
        const iconMenu = document.querySelector('.js-burger');
        const menuBody = document.querySelector('.js-burger-menu');
        const logo = document.querySelector('.js-logo');
        const noScroll = document.querySelector('.js-wrapper');
        items.forEach(item => {
            item.addEventListener('click', activeToggle)
        });
        function activeToggle (){
            items.forEach(item => {
                content.classList.add('active');
                item.classList.remove('active');
            });
            this.classList.add('active');
            const tabName = this.getAttribute('data-name');
            selectTabContent(tabName);
        }
        function selectTabContent(tabName) {
            assortment.forEach(item => {
                item.classList.contains(tabName) ? item.classList.add('active') : item.classList.remove('active')
            });
            links.forEach(item => {
                item.addEventListener('click', closeContent)
            });
        }
        function closeContent() {
            content.classList.remove('active');
            iconMenu.classList.remove('active');
            menuBody.classList.remove('active');
            logo.classList.remove('active');
            noScroll.classList.remove('active');
            assortment.forEach(item => {
                item.classList.remove('active')
            });
            items.forEach(item => {
                item.classList.remove('active');
            });
        }
    }
}
activeLink();
new Swiper('.socials-slider', {
    navigation: {
         nextEl: '.socials-slider__next',
         prevEl: '.socials-slider__prev ',
     },
     pagination: {
         el: '.swiper-pagination',
         clickable: true,
     },
     keyboard: {
        enabled: true,
        onlyInViewport: true,
     },
     loop: true,
     autoHeight: true,
     grabCursor: true,
     spaceBetween: 25,
     speed: 1000,
     centeredSlides: true,
     slidesPerView: 'auto',
     breakpoints: {
        992: {
            spaceBetween: 25,
        },
        767: {
            spaceBetween: 15,
        },
        320: {
            spaceBetween: 10,
        }
    }
});
